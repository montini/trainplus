import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserReg } from '../_model/userReg';
import { BackendConnectionService } from '../services/backend-connection.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  user:UserReg;
  validated:boolean;
  attempt: boolean = false;
  regOK: boolean = false;
  regFail: boolean = false;



  constructor(public activeModal: NgbActiveModal, private backend: BackendConnectionService) {
    this.validated = false;
    this.user = new UserReg();
  }


  ngOnInit(): void {
  }

  register() {
    this.attempt = true;
    this.validated = true;
    if (this.user.validate()) {
      this.backend.register(this.user).subscribe(res => {
        this.regOK = true;
        setTimeout(() => this.activeModal.close('Close click'), 4000);
      }, error => {
        this.regFail = true;
        this.attempt = false;
        setTimeout(() => this.regFail = false, 4000);
      });
    } else {
      this.attempt = false;
    }

  }

}
