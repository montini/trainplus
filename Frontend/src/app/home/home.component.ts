import { Component } from '@angular/core';
import {NgbDateStruct, NgbTimeStruct, NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import { BackendConnectionService } from '../services/backend-connection.service'
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { Trip } from '../_model/trip';
import { TripInfo } from '../_model/tripInfo';
const stations = [];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})



export class HomeComponent {

  trip:Trip = new Trip();
  formatter = (result: string) => result.charAt(0).toUpperCase() + result.slice(1);
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term === '' ? []
    : stations.map(s => s.name).filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  );
  loadAPI: Promise<any>;

  errorMessage: string;
  notFound: boolean;
  loading:boolean = false;

  news: {titolo:string, data:string, primoPiano:boolean, testo:string}[] = [];
  tripInfo: TripInfo[];

  constructor(private backend: BackendConnectionService) {
    let now: Date= new Date();
    this.trip.date = {year: now.getFullYear(), month: now.getMonth()+1, day: now.getDate()}
    this.trip.time = {hour: now.getHours(), minute: now.getMinutes(), second: 0}
    this.trip.origin = "FANO";
    this.trip.destination = "MILANO CENTRALE";

    this.updateNews();

    this.loadAPI = new Promise((resolve) => {
      this.loadScript();
      resolve(true);
    });

    this.backend.getLocalStations().subscribe(result => {
      for (let i = 0; i < result.length; i++) {
        stations[i] = {name: result[i]["name"], id: result[i]["id"]};
      }
    });
  }

  public loadScript() {
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
      if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
        isFound = true;
      }
    }

    if (!isFound) {
      var dynamicScripts = ["https://platform.twitter.com/widgets.js"];

      for (var i = 0; i < dynamicScripts .length; i++) {
        let node = document.createElement('script');
        node.src = dynamicScripts[i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
      }

    }
  }

  updateNews() {
    this.backend.getNews().subscribe(result => {
      this.news = result.data;
      for (let n of this.news) {
        let d = new Date(n.data);
        n.data = d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
      }
    })
  }

  searchSolution() {
    this.loading = true;
    this.tripInfo = null;
    let d:NgbDateStruct = this.trip.date;
    let t:NgbTimeStruct = this.trip.time;

    let origin = stations.find(s => s.name == this.trip.origin);
    let destination = stations.find(s => s.name == this.trip.destination);

    if (origin && destination) {
      let info = {
        origin: origin.id.substring(2),
        destination: destination.id.substring(2),
        time: d.year + "-" + d.month + "-" + d.day + "T" + t.hour + ":" + t.minute
      };

      this.backend.searchSolution(info).subscribe(result => {
        this.tripInfo = result.data;
        this.loading = false;
      }, error => {
        this.showError();
      });
    } else {
      this.showError();
    }

  }

  showError() {
    this.loading = false;
    this.notFound = true;
    this.errorMessage = "I dati inseriti non sono corretti";
    setTimeout(() => this.notFound = false, 4000);
  }

  getBadgeClass(changes: number) {
    switch (changes) {
      case 0:
      return "badge-success";
      case 1:
      return "badge-warning";
      default:
      return "badge-danger";
    }
  }





}
