import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendConnectionService } from '../services/backend-connection.service';
import { TrainInfo } from '../_model/trainInfo';
import { StatusStation } from '../_model/statusStation';

const stations = [];

@Component({
  selector: 'app-train-map',
  templateUrl: './train-map.component.html',
  styleUrls: ['./train-map.component.css']
})
export class TrainMapComponent {

  loaded:boolean = false;
  @Input() trainInfo:TrainInfo;

  constructor(public activeModal: NgbActiveModal, private backend: BackendConnectionService) {
    this.backend.getLocalStations().subscribe(result => {
      for (let i = 0; i < result.length; i++) {
        stations[i] = {name: result[i]["name"], id: result[i]["id"], lat: result[i]["lat"], lon: result[i]["lon"], region: result[i]["region"]};
      }
      this.loaded = true;
    });
  }

  getLat(id:string) {
    return this.getStation(id).lat;
  }

  getLon(id:string) {
    return this.getStation(id).lon;
  }

  private getStation(id:string):any {
    let s = stations.find(s => s.id == id);
    if (s) {
      return s;
    } else {
      return {name:"", id:"", lat:0, len:0};
    }
  }

  getOpacity(s:StatusStation) {
    return (s.departure[0] != "-" || s.arrival[0] != "-") ? 1 : 0.6;
  }

}
