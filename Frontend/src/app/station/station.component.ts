import { Component, OnInit } from '@angular/core';
import { StationFavourite } from '../_model/stationFavourite';
import { StationInfo } from '../_model/stationInfo';
import { ActivatedRoute } from '@angular/router';
import { BackendConnectionService } from '../services/backend-connection.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators/map';
import { DataService } from '../services/shared-data.service';
import { ViewChild } from '@angular/core';
import { NgbTooltip, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RichStationFavourite } from '../_model/richStationFavourite';
import { StationMapComponent } from '../station-map/station-map.component';
import { Router } from '@angular/router';
const stations = [];


@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent {

  @ViewChild('t') public tooltip: NgbTooltip;

  currentJustify = 'fill';

  isLoggedIn:boolean;
  stationName:string = "";
  favourites: RichStationFavourite[];

  stationInfo: StationInfo;

  loading: boolean;

  errorMessage: string;
  notFound: boolean;
  formatter = (result: string) => result.charAt(0).toUpperCase() + result.slice(1);
  search = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term === '' ? []
    : stations.map(s => s.name).filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  );

  constructor(private route: ActivatedRoute, private backend: BackendConnectionService, private ds: DataService, private modalService: NgbModal,
  private router: Router) {
    this.backend.getLocalStations().subscribe(result => {
      for (let i = 0; i < result.length; i++) {
        stations[i] = {name: result[i]["name"], id: result[i]["id"], lat: result[i]["lat"], lon: result[i]["lon"], region: result[i]["region"]};
      }

      this.route.queryParams.subscribe(params => {
        if (params["name"]) {
          this.setAndSearch(params["name"]);
        }
      });

      this.ds.getStatus().subscribe(status => {
        this.isLoggedIn = status;
        if (this.isLoggedIn) {
          this.updateFavourites();
        }
      });
    });
  }


  updateFavourites() {
    this.backend.getStationFavourites().subscribe(result => {
      let favs:RichStationFavourite[] = [];
      for (let r of result.data) {
        let fav: RichStationFavourite = this.enrichStation(r);
        favs.push(fav);
      }
      this.favourites = favs;
      this.isLoggedIn = true;
    }, error => {
      if (error.status == 403) {
        this.isLoggedIn = false;
      }
    });
  }

  enrichStation(station):RichStationFavourite {
    let rich: RichStationFavourite = new RichStationFavourite();
    if (station.cod) {
      rich.cod = station.cod;
    }
    rich.name = station.name;
    rich.isFavourite = true;
    let detail = stations.find(s => s.name == station.name);
    if (detail) {
      rich.region = detail.region;
      rich.lat = detail.lat;
      rich.lon = detail.lon;
    }
    return rich;
  }

  setFavourite(station:StationFavourite, fav:boolean) {
    station.isFavourite = fav;
    this.backend.setStationFavourite(station).subscribe(res => {
      if (res.status == 201) {
        this.updateFavourites();
      }
    });
  }

  newFavourite(station:StationInfo) {
    if (this.favourites.findIndex(f => f.name == station.name) != -1) {
      const isOpen = this.tooltip.isOpen();
      if (!isOpen) {
        this.tooltip.open("AA");
        setTimeout(() => this.tooltip.close(), 3200);
      }
    } else {
      let fav:StationFavourite = {
        cod: station.code,
        name: station.name,
        isFavourite: true
      }
      this.setFavourite(fav, true);
    }
  }

  checkAlreadyFav(station:StationInfo):boolean {
    return false;
  }

  searchStation() {
    this.loading = true;
    this.notFound = false;
    this.stationInfo = null;
    let station = stations.find(s => s.name == this.stationName);

    if (!station) {
      this.showError();
    } else{
      let cod:string = station.id;
      this.backend.getStation({cod_stazione: cod, name: this.stationName}).subscribe(result => {
        this.stationInfo = result.data;
        this.loading = false;
      }, error => {
        this.showError();
      });
    }
  }

  searchTrain(trainNumber:number) {
    this.router.navigateByUrl("/train?code="+trainNumber);
  }

  showError() {
    this.loading = false;
    this.notFound = true;
    let name:string = this.stationName;
    this.errorMessage = "La stazione " + name + " non è stata trovata. Selezionane una tra le suggerite"
    setTimeout(() => this.notFound = false, 4000);
  }


  setAndSearch(name:string) {
    this.stationName = name;
    this.searchStation();
  }

  openMap(station:RichStationFavourite) {
    const modalRef = this.modalService.open(StationMapComponent);
    modalRef.componentInstance.station = this.enrichStation(station);
  }

}
