import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendConnectionService } from './services/backend-connection.service'
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap'
import { RegistrationComponent } from './registration/registration.component';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from './services/shared-data.service';
import { Subscription } from 'rxjs';
import { Socket } from 'ng-socket-io';
import { TrainNotification } from './_model/notification';
import { PushNotificationsService } from 'ng-push';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';
  user: { logemail: string, logpassword: string } = {
    logemail: 'test',
    logpassword: '1234'
  };

  loggedIn:boolean = false;
  userName:string;
  loginFail = false;
  errorMsg = "";
/*
  notificationMessage:string;
  showNotification:boolean;
*/
  constructor(private backend: BackendConnectionService, private modalService: NgbModal,
    private router: Router, private ds: DataService, /*private socket :Socket,*/ private push: PushNotificationsService) {
    this.backend.getSession().subscribe(result => {
      if (result.status == 200) {
        this.loggedIn = true;
        this.ds.setStatus(true);
        this.userName = result.data["username"];
        this.backend.getSubscriptions().subscribe(res => {
          if (res.data.length > 0) {
            setInterval(this.pollSubscriptions(), 500);
          }
        });
      }
    });
    //this.sendNotification();
    //this.receiveNotification();

  }

  pollSubscriptions() {
    this.backend.pollSubscriptions().subscribe(res => {
      for(var k in res.data) {
        if (res.data[k].trigger) {
          console.log(res.data[k]);
          let not = res.data[k].info;
          let notificationMessage:string = "Il treno " + not.trainNumber + " è arrivato alla stazione di " + not.stationName;
          if (res.data[k].info.delay > 0) {
            notificationMessage += " con un ritardo di " + res.data[k].info.delay + " minuti."
          }
          this.push.create('Treno in arrivo!', {body: notificationMessage, icon: '/assets/logo.png'}).subscribe(
              res => console.log(res),
              err => console.log(err)
          );
        }
      }
    });
  }

  /*receiveNotification(): void {
    this.socket.on('new notification', function(data){
      console.log("newNotification")
      //alert(data.last_update_location + " " + data.last_update_time + " "+ data.delay)
    });
  }*/

  login() {
    this.loginFail = false;
    this.backend.login(this.user).subscribe(result => {
      if (result.status == 200) {
        this.loggedIn = true;
        this.userName = result.data["username"];
        this.ds.setStatus(true);
      }
    }, error => {
      this.loginFail = true;
      if (error.status == 401) {
        this.errorMsg = "Combinazione email/password errata.";
      } else {
        this.errorMsg = "Si è verificato un errore di sistema.";
      }
    });
  }

  logout() {
    this.backend.logout().subscribe(result => {
      this.loggedIn = false;
      this.ds.setStatus(false);
    });
  }

  openRegistration() {
    this.modalService.open(RegistrationComponent);
  }

  /*sendNotification(){
    this.socket.emit('create notification','Notification Test');
  }*/
}
