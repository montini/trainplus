import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserReg } from '../_model/userReg';
import { BackendConnectionService } from '../services/backend-connection.service';
import { Input } from '@angular/core';
import { RichStationFavourite } from '../_model/richStationFavourite';



@Component({
  selector: 'app-registration',
  templateUrl: './station-map.component.html',
  styleUrls: ['./station-map.component.css']
})
export class StationMapComponent {

  @Input() station:RichStationFavourite;

  constructor(public activeModal: NgbActiveModal) {

  }


}
