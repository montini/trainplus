import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TrainInfo } from '../_model/trainInfo';
import { StationInfo } from '../_model/stationInfo';
import { TrainFavourite } from '../_model/trainFavourite';
import { StationFavourite } from '../_model/stationFavourite';
import { UserReg } from '../_model/userReg';
import { BackendItem } from '../_model/backendItem';
import { TripInfo } from '../_model/tripInfo';
import { TrainSub } from '../_model/trainSub';
import { TrainNotification } from '../_model/notification';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/x-www-form-urlencoded',
    //'withCredentials': 'true',
  })
};

const api = "http://localhost:3000/api/trainplus";


@Injectable({
  providedIn: 'root'
})
export class BackendConnectionService {

  constructor(private http: HttpClient) { }

  register(user):Observable<BackendItem<Object>> {
    return this.http.post<BackendItem<Object>>(api + "/register", user);
    //Se andato a buon fine ritorna true e fa login?
  }

  //Richiede l'accesso, ritorna qualcosa per la sessione?
  login(user):Observable<BackendItem<Object>> {
    return this.http.post<BackendItem<Object>>(api + "/login", user, {withCredentials: true});
  }

  logout():Observable<BackendItem<Object>> {
    return this.http.post<BackendItem<Object>>(api + "/logout", "", {withCredentials: true});
  }

  getSession():Observable<BackendItem<any>> {
    return this.http.get<BackendItem<Object>>(api + "/session", {withCredentials: true});
  }

  searchSolution(request):Observable<BackendItem<TripInfo[]>> {
    return this.http.get<BackendItem<TripInfo[]>>(api + "/solutions", {params: request});
  }

  getTrain(code):Observable<BackendItem<TrainInfo>> {
    return this.http.get<BackendItem<TrainInfo>>(api + "/trainInfo", {params: code});//, {withCredentials: true});
  }

  getTrainFavourites():Observable<BackendItem<TrainFavourite[]>> {
    return this.http.get<BackendItem<TrainFavourite[]>>(api + "/trainFavourite", {withCredentials: true});
  }

  setTrainFavourite(train: TrainFavourite):Observable<BackendItem<string>> {
    //Probabilmente la struttura sarà una coppia con trainFavourite e userID.
    //Se sale con isFav = true, va aggiunto, altrimenti va rimosso dai preferiti
    return this.http.post<BackendItem<string>>(api + "/insertTrain", {cod: train.num, isFav: train.isFavourite}, {withCredentials: true});
  }


  getStation(code):Observable<BackendItem<StationInfo>> {
    return this.http.get<BackendItem<StationInfo>>(api + "/stationInfo", {params: code});
  }

  getStationFavourites():Observable<BackendItem<StationFavourite[]>> {
    return this.http.get<BackendItem<StationFavourite[]>>(api + "/stationFavourite", {withCredentials: true});
  }

  setStationFavourite(station: StationFavourite):Observable<BackendItem<StationFavourite>> {
    //Probabilmente la struttura sarà una coppia con stationFavourite e userID.
    //Se sale con isFav = true, va aggiunto, altrimenti va rimosso dai preferiti
    return this.http.post<BackendItem<StationFavourite>>(api + "/insertStation", {code: station.cod, name: station.name, isFav: station.isFavourite}, {withCredentials: true});
  }

  getNews():Observable<BackendItem<any>> {
    return this.http.get<BackendItem<any>>(api + "/news");
  } //OK, anche se in inglese e vanno gestite le date in ms

  getLocalStations() {
    return this.http.get<string>("/assets/stations.json");
  }

  setSubNotification(sub:TrainSub):Observable<BackendItem<boolean>> {
    return this.http.post<BackendItem<boolean>>(api + "/insertSubscription", sub, {withCredentials: true});
  }

  getSubscriptions():Observable<BackendItem<TrainSub[]>> {
    return this.http.get<BackendItem<TrainSub[]>>(api + "/subscriptions", {withCredentials: true});
  }

  pollSubscriptions():Observable<BackendItem<TrainNotification>> {
    return this.http.get<BackendItem<TrainNotification>>(api + "/pollSubscriptions", {withCredentials: true});
  }

}
