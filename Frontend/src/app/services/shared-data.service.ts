import { Injectable } from '@angular/core';
import { Observable,BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {
  private isLoggedIn = new BehaviorSubject<boolean>(false);

  setStatus(message: boolean) {
    this.isLoggedIn.next(message);
  }

  getStatus(): Observable<boolean> {
    return this.isLoggedIn.asObservable();
  }

  getStatusNow(): boolean {
    return this.isLoggedIn.getValue();
  }
}
