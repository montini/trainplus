import {NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';

export class Trip {
  origin: string;
  destination: string;
  date: NgbDateStruct;
  time: NgbTimeStruct;
}
