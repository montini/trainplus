import { TransitTrain } from "./transitTrain";

export class StationInfo {
  name:string; //Nome
  code:string; //Codice
  departure: TransitTrain[]; //Treni in partenza
  arrival: TransitTrain[]; //Treni in arrivo
}
