export class StatusStation {
  name:string; //Nome della Stazione
  id: string; //ID della stazione
  est_arrival:string; //Arrivo stimato
  est_departure:string; //Partenza stimata
  est_track:number; //Binario stimato
  arrival:string; //Arrivo
  departure:string; //Partenza
  track:number; //Binario
}
