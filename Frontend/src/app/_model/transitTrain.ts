export class TransitTrain {
  name: string; // Nome del treno (Regionale/FrecciaRossa)
  code: string; // Codice
  station: string;  // Stazione, può essere di origine o di destinazione
  time: string; // Orario, può essere di arrivo o di partenza
  track: number; // Binario
  delay: number; // Ritardo
}
