export class BackendItem<T> {
  status: number;
  text: string;
  data: T;
}
