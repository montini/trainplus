export class UserReg {
  public email:{value:string, isValid:boolean} = {value: "", isValid: false};
  public username:{value:string, isValid:boolean} = {value: "", isValid: false};
  public password1:{value:string, isValid:boolean} = {value: "", isValid: false};
  public password2:{value:string, isValid:boolean} = {value: "", isValid: false};

  constructor() {
    this.email.isValid = false;
    this.username.isValid = false;
    this.password1.isValid = false;
    this.password2.isValid = false;
  }

  validateEmail() {
    this.email.isValid = (this.email.value.length > 6 && this.email.value.indexOf("@")> -1 && this.email.value.indexOf(".")> -1);
  }

  validateUsername() {
    this.username.isValid = (this.username.value.length > 3);
  }

  validatePassword1() {
    this.password1.isValid = (this.password1.value.length > 5);
  }

  validatePassword2() {
    this.password2.isValid = (this.password1.value.length > 0 && this.password1.value == this.password2.value);
  }

  validate():boolean {
    this.validateEmail();
    this.validateUsername();
    this.validatePassword1();
    this.validatePassword2();
    return (this.email.isValid && this.username.isValid && this.password1.isValid && this.password2.isValid);
  }


}
