export class StationFavourite {
  cod:string; //Codice stazione
  name:string; //Nome
  isFavourite:boolean; //Se tra i preferiti, per riaggiornare il frontend (true di default al download)
}
