import { StatusStation } from "./statusStation";

export class TrainInfo {
  name: string; // Nome treno
  code: number; // Codice
  origin: string; // Staizone di origine
  start_time: string; // Partenz dall'origine
  destination: string; // Stazione di arrivo
  arrival_time: string; // Orario di arrivo
  last_update_location: string; // Ultimo aggiornamento (stazione)
  last_update_time: string;  // Ultimo aggiornamento (tempo)
  status: StatusStation[];  // Stazioni visitate
}
