export class TrainFavourite {
  num:number; //Numero treno
  origin:string; //Origine
  destination:string; //Destinazione
  isFavourite:boolean; //Se ancora tra preferiti, per frontend (true di default al download)
}
