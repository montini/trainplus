export class TrainSub {
  trainNumber: number;
  stationName: string;
  sub: boolean;

  constructor (stationName: string, trainNumber:number, sub:boolean) {
    this.stationName = stationName;
    this.trainNumber = trainNumber;
    this.sub = sub;
  }

}
