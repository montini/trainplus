import { TripInfoTrain } from "./tripInfoTrain";

export class TripInfo {
  origin: string;
  destionation: string;
  departure_time: string;
  arrival_time: string;
  duration: string;
  changes: number;
  trains: TripInfoTrain[]; //vehicles
}
