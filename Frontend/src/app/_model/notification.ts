export class TrainNotification {
  trigger: boolean;
  info: {
    stationName: string;
    trainNumber: number;
    delay: number;
  }
}
