export class TripInfoTrain {
  name: string; //categoriaDescrizione + numeroTreno
  departure_station: string;
  departure_time: string;
  arrival_station: string;
  arrival_time: string;
  duration: string;
}
