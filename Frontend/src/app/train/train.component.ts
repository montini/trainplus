import { Component, OnInit } from '@angular/core';
import { TrainFavourite } from '../_model/trainFavourite';
import { TrainInfo } from '../_model/trainInfo';
import { StatusStation } from '../_model/statusStation';
import { ActivatedRoute } from '@angular/router';
import { BackendConnectionService } from '../services/backend-connection.service';
import { DataService } from '../services/shared-data.service';
import { NgbTooltip, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { TrainMapComponent } from '../train-map/train-map.component';
import { TrainSub } from '../_model/trainSub';
import { PushNotificationsService } from 'ng-push';
import { Router } from '@angular/router';

@Component({
  selector: 'app-train',
  templateUrl: './train.component.html',
  styleUrls: ['./train.component.css']
})
export class TrainComponent implements OnInit {


  isLoggedIn:boolean;
  trainNumber:number;
  favourites: TrainFavourite[];
  subscriptions: TrainSub[] = [];
  trainInfo: TrainInfo;

  notFound:boolean;

  errorMessage: string;
  loading: boolean = false;
  @ViewChild('t') public tooltip: NgbTooltip;

  constructor(private route: ActivatedRoute, private backend: BackendConnectionService, private router: Router,
    private ds: DataService, private modalService: NgbModal, private push: PushNotificationsService) {

    this.route.queryParams.subscribe(params => {
      if (params["code"]) {
        this.setAndSearch(params["code"]);
      }
    });

    this.ds.getStatus().subscribe(status => {
      this.isLoggedIn = status;
      if (status) {
        this.updateFavourites();
        this.getSubscriptions();
      }
    });

  }

  ngOnInit(): void {
  }


  updateFavourites() {
    this.backend.getTrainFavourites().subscribe(result => {
      for (let r of result.data) {
        r.isFavourite = true;
      }
      this.favourites = result.data;
      this.isLoggedIn = true;
    }, error => {
      if (error.status == 403) {
        this.isLoggedIn = false;
      }
    });
  }

  setFavourite(train:TrainFavourite, fav:boolean) {
    train.isFavourite = fav;
    this.backend.setTrainFavourite(train).subscribe(res => {
      if (res.status == 201) {
        this.updateFavourites();
      }
    });
  }

  newFavourite(train:TrainInfo) {
    if (this.favourites.findIndex(f => f.num == train.code) != -1) {
      const isOpen = this.tooltip.isOpen();
      if (!isOpen) {
        this.tooltip.open("AA");
        setTimeout(() => this.tooltip.close(), 3200);
      }
    } else {
      let fav:TrainFavourite = {
        num: train.code,
        origin: train.origin,
        destination: train.destination,
        isFavourite: true
      }
      this.setFavourite(fav, true);
    }
  }

  searchTrain() {
    this.loading = true;
    this.notFound = false;
    this.trainInfo = null;
    this.backend.getTrain({id: this.trainNumber}).subscribe(result => {
      this.trainInfo = result.data;
      this.loading = false;
    }, error => {
      let code:number = error.status;
      this.notFound = true;
      let num:string = (this.trainNumber ? this.trainNumber+"" : "");
      this.errorMessage = "Il treno " + num + " non è stato trovato."
      setTimeout(() => this.notFound = false, 4000);
      this.loading = false;
    });
  }

  setAndSearch(number:number) {
    this.trainNumber = number;
    this.searchTrain();
  }

  searchStation(stationName:string) {
    this.router.navigateByUrl("/station?name="+stationName);
  }

  timeHighlightArr(station:StatusStation) {
    return this.timeHighlight(station.est_arrival, station.arrival);
  }

  timeHighlightDep(station:StatusStation) {
    return this.timeHighlight(station.est_departure, station.departure);
  }

  timeHighlight(est:string, fin:string):string {
    if (fin[0] == "-") return "text-dark";
    let diff:number = (this.timeToDate(fin) - this.timeToDate(est))/60000;
    return (diff > 2.0 ? "text-danger" : "text-success");
  }

  timeToDate(time:string):number {
    if (time.length == 5 && time[0] != "-") {
      let d = new Date();
      d.setHours(+time.substr(0,2));
      d.setMinutes(+time.substr(3,2));
      return d.valueOf();
    } else {
      return 0;
    }
  }

  openMap(trainNumber:number, fromCurrent: boolean) {
    let info:TrainInfo;
    if (fromCurrent) {
      info = this.trainInfo;
      const modalRef = this.modalService.open(TrainMapComponent);
      modalRef.componentInstance.trainInfo = this.trainInfo;
    } else {
      this.backend.getTrain({id: trainNumber}).subscribe(result => {
        const modalRef = this.modalService.open(TrainMapComponent);
        modalRef.componentInstance.trainInfo = result.data;
      });
    }
  }

  subNotification(stationName:string, trainNumber:number) {
    let sub: TrainSub = new TrainSub(stationName, trainNumber, true);
    this.backend.setSubNotification(sub).subscribe(res => {
      this.subscriptions.push(sub);
          this.push.requestPermission();
          this.ngOnInit();
    });
  }

  remNotification(stationName:string, trainNumber:number) {
    let sub: TrainSub = new TrainSub(stationName, trainNumber, false);
    this.backend.setSubNotification(sub).subscribe(res => {
      sub.sub = true;
      var el = this.subscriptions.filter(s => s.stationName == stationName).filter(s => s.trainNumber == trainNumber);
      let index = this.subscriptions.indexOf(el[0]);
      if (index > -1) {
        this.subscriptions.splice(index, 1);
      }
    });
  }

  getSubscriptions() {
    this.backend.getSubscriptions().subscribe(res => {
      console.log(res.data)
      this.subscriptions = res.data;
    });
  }

  hasSubscriptions(trainNumber:number) {
    return this.subscriptions.map(s => s.trainNumber).includes(trainNumber);
  }

  isSubscribed(stationName:string, trainNumber:number) {
    return this.subscriptions.filter(s => s.trainNumber == trainNumber).map(s => s.stationName).includes(stationName);
  }

  getSubscriptionsForTrain(trainNumber) {
    return this.subscriptions.filter(s => s.trainNumber == trainNumber);
  }
}
