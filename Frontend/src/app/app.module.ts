import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatIconModule } from "@angular/material/icon";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { PushNotificationsModule } from 'ng-push';

import { HomeComponent } from './home/home.component';
import { StationComponent } from './station/station.component';
import { TrainComponent } from './train/train.component';
import { RegistrationComponent } from './registration/registration.component';
import { DataService } from './services/shared-data.service';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:8000', options: {} };

import { StationMapComponent } from './station-map/station-map.component';
import { TrainMapComponent } from './train-map/train-map.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'station', component: StationComponent },
  { path: 'train', component: TrainComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StationComponent,
    TrainComponent,
    RegistrationComponent,
    StationMapComponent,
    TrainMapComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAARF1EvKVGOu2qBTnwLhAR74Rnm5taxuI'
    }),
    PushNotificationsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent],
  entryComponents: [RegistrationComponent, StationMapComponent, TrainMapComponent]
})
export class AppModule { }
