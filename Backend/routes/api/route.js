var express = require('express');

var router = express.Router();

var Controller = require('../../controllers/controller');

//GESTIONE UTENTE
//autenticazione - registrazione - eliminazione - logout - controllo sessione utente
router.post('/login', Controller.authenticateUser);
router.post('/register', Controller.createUser);
router.delete('/:id',Controller.removeUser);
router.post('/logout', Controller.logout);
router.get('/session', Controller.getSession);

//GESTIONE PREFERITI
//inserimento treno preferito
router.post('/insertTrain', Controller.insertUserFavTrain);
//inserimento stazione preferita
router.post('/insertStation', Controller.insertUserFavStation);
//acquisizione treni preferiti
router.get('/trainFavourite', Controller.getUserFavTrain);
//acquisizione stazioni preferite
router.get('/stationFavourite', Controller.getUserFavStation);
//inserimento sottoscrizione per notifiche
router.post('/insertSubscription', Controller.insertSubscription);
//acquisizione sottoscrizioni utente
router.get('/subscriptions', Controller.getUserSubscriptions);
//acquisizione notifiche se disponibili
router.get('/pollSubscriptions', Controller.getPollSubscriptions);


//GESTIONE INTERAZIONE CON TRENITALIA
//ricerca viaggio
router.get('/solutions', Controller.getSolutions);
//ricerca informazioni treno
router.get('/trainInfo', Controller.getTrainInfo);
//ricerca informazioni stazione
router.get('/stationInfo', Controller.getStationInfo);
//acquisizione news
router.get('/news', Controller.getNews);

module.exports = router;
