var GeneralService = require('../services/general.service');
var TrenitaliaService = require('../services/trenitalia.service');
var User = require('../models/user');
var Message = require('../models/utils/messages');

_this = this

function checkSession(req){
  return req.session.userId;
}

exports.getSession = async function(req, res, next){
  if(checkSession(req)) {
    try{
        var ret = await GeneralService.getLoggedUser(req.session.userId);
        return res.status(200).json({status: 200, data: ret, message: Message.SESSION_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.SESSION_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.getTrainInfo = async function(req, res, next) {
    try{
        var ret = await TrenitaliaService.getTrainInfo(req.query);
        return res.status(200).json({status: 200, data: ret, message: Message.TRAIN_INFO_OK});
    } catch(e){
        return res.status(400).json({status: 400, message: Message.TRAIN_INFO_NOT_OK});
    }
}

exports.getSolutions = async function(req, res, next){
    try{
        var ret = await TrenitaliaService.getSolutions(req.query);
        return res.status(200).json({status: 200, data: ret, message: Message.SOLUTION_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.SOLUTION_NOT_OK});
    }
}

exports.getNews = async function(req, res, next) {
    try{
        var ret = await TrenitaliaService.getNews();
        return res.status(200).json({status: 200, data: ret, message: Message.NEWS_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.NEWS_NOT_OK});
    }
}

exports.getStationInfo = async function(req, res, next) {
    try{
        var ret = await TrenitaliaService.getStationInfo(req.query);
        return res.status(200).json({status: 200, data: ret, message: Message.STATION_INFO_OK});
    } catch(e){
        return res.status(400).json({status: 400, message: Message.STATION_INFO_NOT_OK});
    }
}

exports.insertUserFavTrain = async function(req, res, next) {
  if(checkSession(req)) {
    var trainData = {
      cod: req.body.cod,
      user_id: req.session.userId,
    }
    try{
        if(req.body.isFav) {
          var insertedTrain = await TrenitaliaService.insertUserFavTrain(trainData);
          return res.status(201).json({status: 201, data: insertedTrain, message: Message.FAV_TRAIN_OK});
        } else {
          var deletedTrain = await TrenitaliaService.removeUserFavTrain(trainData);
          return res.status(200).json({status: 200, data: deletedTrain, message: Message.FAV_TRAIN_DEL_OK});
        }
    }catch(e){
        return res.status(400).json({status: 400, data: e, message: Message.FAV_TRAIN_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.getUserFavTrain = async function(req, res, next) {
  if(checkSession(req)) {
    var param = {
      user_id: req.session.userId,
    }
    try{
        var preferredTrain = await TrenitaliaService.getUserFavTrains(param);
        return res.status(200).json({status: 200, data: preferredTrain, message: Message.FAV_TRAIN_GET_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.FAV_TRAIN_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.insertUserFavStation = async function(req, res, next) {
  if(checkSession(req)) {
    var stationData = {
      cod: req.body.code,
      name: req.body.name,
      user_id: req.session.userId,
    }
    try{
      if(req.body.isFav) {
        var insertedStation = await TrenitaliaService.insertUserFavStation(stationData);
        return res.status(201).json({status: 201, data: insertedStation, message: Message.FAV_STATION_OK});
      } else {
        var deletedStation = await TrenitaliaService.removeUserFavStation(stationData);
        return res.status(200).json({status: 200, data: deletedStation, message: Message.FAV_STATION_DEL_OK});
      }
    }catch(e){
        return res.status(400).json({status: 400, message: Message.FAV_STATION_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.getUserFavStation = async function(req, res, next) {
  if(checkSession(req)) {
    var param = {
      user_id: req.session.userId,
    }
    try{
        var preferredStation = await TrenitaliaService.getUserFavStations(param);
        return res.status(200).json({status: 200, data: preferredStation, message: Message.FAV_STATION_GET_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.FAV_STATION_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.insertSubscription = async function(req, res, next) {
  if(checkSession(req)) {
    var subData = {
      user_id : req.session.userId,
      stationName : req.body.stationName,
      trainNumber: req.body.trainNumber,
    }
    console.log(req.body.sub);
    try{
      if(req.body.sub) {
        var insertedSub = await TrenitaliaService.insertSubscription(subData);
        return res.status(201).json({status: 201, data: insertedSub, message: Message.SUB_OK});
      } else {
        var deletedSub = await TrenitaliaService.removeSubscription(subData);
        return res.status(200).json({status: 200, data: deletedSub, message: Message.SUB_DEL_OK});
      }
    }catch(e){
        return res.status(400).json({status: 400, message: Message.SUB_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.getUserSubscriptions = async function(req, res, next) {
  if(checkSession(req)) {
    var param = {
      user_id: req.session.userId,
    }
    try{
        var subscriptions = await TrenitaliaService.getUserSubscriptions(param);
        return res.status(200).json({status: 200, data: subscriptions, message: Message.SUB_GET_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.SUB_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.getPollSubscriptions = async function(req, res, next) {
  if(checkSession(req)) {
    var param = {
      user_id: req.session.userId
    }
    try{
        var subscriptions = await TrenitaliaService.getPollSubscriptions(param);
        return res.status(200).json({status: 200, data: subscriptions, message: Message.SUB_POLL_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.SUB_POLL_NOT_OK});
    }
  } else {
    return res.status(403).json({status: 403, message: Message.NOT_LOGGED});
  }
}

exports.createUser = async function(req, res, next){
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.send("Passwords do not match.");
    return next(err);
  }
  if (req.body.email &&
    req.body.username &&
    req.body.password1 &&
    req.body.password2) {

    var userData = {
      email: req.body.email.value,
      username: req.body.username.value,
      password: req.body.password1.value,
      passwordConf: req.body.password2.value,
    }
    try{
        var createdUser = await GeneralService.createUser(userData);
        req.session.userId = createdUser;
        return res.status(201).json({status: 201, data: createdUser, message: Message.USER_CREATION_OK});
    }catch(e){
        return res.status(400).json({status: 400, message: Message.USER_CREATION_NOT_OK});
    }

  } else {
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
}

exports.logout = async function(req, res, next) {
  if (req.session.userId) {
    try {
      var ret = await GeneralService.logout(req);
      return res.status(200).json({status: 200, message: Message.USER_LOGOUT_OK});
    }catch(e){
      return res.status(400).json({status: 400, message: Message.USER_LOGOUT_NOT_OK});
    }
  } else {
    var err = new Error('Session not present');
    err.status = 401;
    return err;
  }
}

exports.authenticateUser = async function(req, res, next){
  if (req.body.logemail && req.body.logpassword) {
    var userData = {
      logemail: req.body.logemail,
      logpassword: req.body.logpassword,
    }
    try{
        var authUser = await GeneralService.authenticateUser(userData, req)
        req.session.userId = authUser._id;
        return res.status(200).json({status: 200, data: authUser, message: Message.USER_AUTH_OK});
    }catch(e){
        return res.status(401).json({status: 401, message: Message.USER_AUTH_NOT_OK});
    }
  } else {
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
}

exports.removeUser = async function(req, res, next){
    try{
        var deleted = await GeneralService.deleteUser(req.params.id);
        return res.status(200).json({status:200, data: deleted, message: Message.USER_DEL_OK})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }
}
