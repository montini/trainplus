var mongoose = require('mongoose');

var StationSchema = new mongoose.Schema({
    cod: {
      type: String,
      unique: true,
      required: true
    },
    name: {
      type: String,
      unique: true,
      required: true
    },
    user_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

var Station = mongoose.model('Station', StationSchema);
module.exports = Station;
