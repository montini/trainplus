var mongoose = require('mongoose');

var SubscriptionSchema = new mongoose.Schema({
    stationName: {
      type: String,
      required: true
    },
    trainNumber : { type: Number, ref: 'Train' },
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

var Subscription = mongoose.model('Subscription', SubscriptionSchema);
module.exports = Subscription;
