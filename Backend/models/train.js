var mongoose = require('mongoose');

var TrainSchema = new mongoose.Schema({
    num: {
      type: Number,
      unique: true,
      required: true
    },
    origin: {
      type: String,
      required: true
    },
    destination: {
      type: String,
      required: true
    },
    notification: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Subscription' }],
    user_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

var Train = mongoose.model('Train', TrainSchema);
module.exports = Train;
