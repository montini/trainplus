const ERR = {
  TRAIN_DEL: "Train could not be deleted",
  TRAIN_DEL_OP: "Error occured while deleting the train",
  TRAIN_CREATION: "Error while creating train",
  STATION_DEL: "Station could not be deleted",
  STATION_DEL_OP: "Error occured while deleting the station",
  STATION_CREATION: "Error while creating station",
  SUB_DEL: "Subscription could not be deleted",
  SUB_DEL_OP: "Error occured while deleting the subscription",
  SUB_CREATION: "Error while creating subscription",
}

module.exports = ERR;
