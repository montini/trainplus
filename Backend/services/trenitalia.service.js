var User = require('../models/user');
var Train = require('../models/train');
var Station = require('../models/station');
var Subscription = require('../models/subscription');
var request = require('sync-request');
var url = "https://www.lefrecce.it/msite/api";
var url_train_info = "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno";
var ErrorMsg = require('../models/utils/error_messages');

_this = this

function encodeQueryData(data) {
   let ret = [];
   for (let d in data)
     ret.push(d + '=' + data[d]);
   return ret.join('&');
}

function get(get_url, endpoint, params) {
  var res = request('GET', get_url + endpoint + params);
  return res.getBody('utf8');
}

function params(req) {
  return encodeQueryData(req);
}

function solutionParams(req, option) {
  return req.id + "/" + option;
}

exports.removeUserFavTrain = async function(train) {
  try{
      var deleted = await Train.remove({ num: train.cod, user_id: train.user_id});
      if(deleted.n === 0){
          throw Error(ErrorMsg.TRAIN_DEL)
      }
      return deleted;
  }catch(e){
      throw Error(ErrorMsg.TRAIN_DEL)
  }
}

exports.insertUserFavTrain = async function(train){
  var info = await this.getTrainInfo({id: train.cod});
  var newTrain = new Train({
      num: train.cod,
      user_id: train.user_id,
      origin: info.origin,
      destination: info.destination,
  });
  try{
    var savedTrain = await Train.create(newTrain)
    return savedTrain;
  }catch(e){
    throw Error(ErrorMsg.TRAIN_CREATION)
  }
}
exports.removeUserFavStation = async function(station) {
  try{
      var deleted = await Station.remove({ cod: station.cod, name: station.name, user_id: station.user_id});
      if(deleted.n === 0){
          throw Error(ErrorMsg.STATION_DEL)
      }
      return deleted;
  }catch(e){
      throw Error(ErrorMsg.STATION_DEL_OP)
  }
}
exports.insertUserFavStation = async function(station) {
      var newStation = new Station({
        cod: station.cod,
        name: station.name,
        user_id: station.user_id,
      });

      try{
        var savedStation = await Station.create(newStation)
        return savedStation;
      }catch(e){
          throw Error(ErrorMsg.STATION_CREATION)
      }
}

exports.getUserFavStations = async function(req) {
    return await Station.find({
                  user_id : req.user_id
                }).exec();
}

exports.getUserFavTrains = async function(req) {
  return await Train.find({
        user_id : req.user_id
      }).exec();
}

exports.insertSubscription = async function(sub) {
  var newSubscription = new Subscription({
    trainNumber: sub.trainNumber,
    stationName: sub.stationName,
    user_id: sub.user_id,
  });

  try{
    var savedSubscription = await Subscription.create(newSubscription)
    return savedSubscription;
  }catch(e){
      throw Error(ErrorMsg.SUB_CREATION)
  }
}

exports.removeSubscription = async function(sub) {
  try{
      var deleted = await Subscription.remove({ trainNumber: sub.trainNumber, stationName: sub.stationName, user_id: sub.user_id});
      if(deleted.n === 0){
          throw Error(SUB_DEL)
      }
      return deleted;
  }catch(e){
      throw Error(SUB_DEL_OP)
  }
}

exports.getUserSubscriptions = async function(req) {
  return await Subscription.find({
        user_id : req.user_id
      }).exec();
}

exports.getPollSubscriptions = async function(req) {
  var userSub = await this.getUserSubscriptions(req);
  var ret = [];
  for(var key in userSub) {
    var trainInfo = this.getTrainInfo({id: userSub[key].trainNumber});
    for(var index in trainInfo.status){
      if(trainInfo.status[index].name == userSub[key].stationName && trainInfo.status[index].arrival != "--:--") {
        var notification = {
          trigger: true,
          info: {
            stationName: userSub[key].stationName,
            trainNumber: userSub[key].trainNumber,
            delay: trainInfo.delay,
          }
        }
        ret.push(notification);
        var deleteSub = await this.removeSubscription({trainNumber: userSub[key].trainNumber, stationName: userSub[key].stationName, user_id : req.user_id});
      }
    }
  }
  return ret;
}

function msToHMS( ms ) {
    // 1- Convert to seconds:
    var seconds = ms / 1000;
    // 2- Extract hours:
    var hours = parseInt( seconds / 3600 ); // 3,600 seconds in 1 hour
    seconds = seconds % 3600; // seconds remaining after extracting hours
    // 3- Extract minutes:
    var min = parseInt( seconds / 60 ); // 60 seconds in 1 minute
    var minutes = min < 10? "0"+min: min;
    // 4- Keep only seconds not extracted to minutes:
    seconds = seconds % 60;
    return hours+":"+minutes;
}

function myDuration(departure, arrival) {

    var hourD = departure.slice(0, 2);
    var minD = departure.slice(3, 5);
    var dep = new Date();
    dep.setHours(hourD);
    dep.setMinutes(minD);

    var hourA = arrival.slice(0, 2);
    var minA = arrival.slice(3, 5);
    var arr = new Date();
    arr.setHours(hourA);
    arr.setMinutes(minA);
    var diff = arr.getTime()<dep.getTime()? dep.getTime() - arr.getTime() : arr.getTime()-dep.getTime();
    return msToHMS(diff);
}
exports.getSolutions = function(req) {
  var endpoint = "/soluzioniViaggioNew/";
  var params = req.origin+"/"+req.destination+"/"+req.time+":00";
  var solutions = JSON.parse(get(url_train_info, endpoint, params));
  var tripInfo = [];

  for(var key in solutions.soluzioni) {
    var departure = solutions.soluzioni[key].vehicles[0].orarioPartenza.slice(11, 16);
    var arrival = solutions.soluzioni[key].vehicles[solutions.soluzioni[key].vehicles.length-1].orarioArrivo.slice(11, 16);
    var sol = {
      origin: solutions.origine,
      destination: solutions.destinazione,
      departure_time: departure,
      arrival_time: arrival,
      duration: myDuration(departure, arrival),
      changes: solutions.soluzioni[key].vehicles.length - 1,
      trains: [],
    }
    for(i = 0; i < solutions.soluzioni[key].vehicles.length; i++) {
      var dep = solutions.soluzioni[key].vehicles[i].orarioPartenza.slice(11, 16);
      var arr = solutions.soluzioni[key].vehicles[i].orarioArrivo.slice(11, 16);
      var veh = {
        name: solutions.soluzioni[key].vehicles[i].categoriaDescrizione+" "+solutions.soluzioni[key].vehicles[i].numeroTreno, //categoriaDescrizione + numeroTreno
        departure_station: solutions.soluzioni[key].vehicles[i].origine,
        departure_time: dep,
        arrival_station: solutions.soluzioni[key].vehicles[i].destinazione,
        arrival_time: arr,
        duration: myDuration(dep, arr),
      }
      sol.trains.push(veh);
    }
    tripInfo.push(sol);
  }
  return tripInfo;
}

function getDepartureStation(req) {
  var endpoint = "/cercaNumeroTreno/";
  return get(url_train_info, endpoint, req);
}

exports.getStationInfo = async function(req) {
  var info = {
    code: req.cod_stazione,
    name: req.name,
    departure: [],
    arrival: [],
  }
  var partenze = await this.getStationDepartures(req);
  for(i = 0; i < partenze.length; i++) {
    var dep = {
      name: partenze[i].compNumeroTreno, // Nome del treno (Regionale/FrecciaRossa)
      code: partenze[i].numeroTreno, // Codice
      station: partenze[i].destinazione,  // Stazione, può essere di origine o di destinazione
      time: partenze[i].compOrarioPartenza, // Orario, può essere di arrivo o di partenza
      track: partenze[i].binarioProgrammatoPartenzaDescrizione, // Binario
      delay: partenze[i].ritardo, // Ritardo
    }
    info.departure.push(dep);
  }

  var arrivi = await this.getStationArrivals(req);
  for(i = 0; i < arrivi.length; i++) {
    var arr = {
      name: arrivi[i].compNumeroTreno, // Nome del treno (Regionale/FrecciaRossa)
      code: arrivi[i].numeroTreno, // Codice
      station: arrivi[i].origine,  // Stazione, può essere di origine o di destinazione
      time: arrivi[i].compOrarioArrivo, // Orario, può essere di arrivo o di partenza
      track: arrivi[i].binarioProgrammatoArrivoDescrizione, // Binario
      delay: arrivi[i].ritardo, // Ritardo
    }
    info.arrival.push(arr);
  }
  return info;
}

exports.getTrainInfo = function(req) {
  var cod_partenza = JSON.parse(getDepartureStation(req.id));
  var endpoint = "/andamentoTreno/"+cod_partenza.codLocOrig+"/";
  var info = JSON.parse(get(url_train_info, endpoint, req.id));
  var ret = {
    name: info.compNumeroTreno, // Nome treno
    code: req.id, // Codice
    origin: info.origine, // Staizone di origine
    start_time: info.compOrarioPartenza, // Partenz dall'origine
    destination: info.destinazione, // Stazione di arrivo
    arrival_time: info.compOrarioArrivo, // Orario di arrivo
    last_update_location: info.stazioneUltimoRilevamento, // Ultimo aggiornamento (stazione)
    last_update_time: info.compOraUltimoRilevamento,  // Ultimo aggiornamento (tempo)
    delay: info.ritardo,
    status: []
  }
  for(i = 0; i < info.fermate.length; i++){
    var fermata = {
      name:info.fermate[i].stazione, //Nome della Stazione
      id:info.fermate[i].id, //ID della Stazione
      est_arrival: info.fermate[i].tipoFermata == 'P'? "--:--" : getTime(info.fermate[i].arrivo_teorico), //Arrivo stimato
      est_departure: info.fermate[i].tipoFermata == 'A'? "--:--" :getTime(info.fermate[i].partenza_teorica), //Partenza stimata
      est_track: info.fermate[i].tipoFermata == 'P'? info.fermate[i].binarioProgrammatoPartenzaDescrizione : info.fermate[i].binarioProgrammatoArrivoDescrizione, //Binario stimato
      arrival: info.fermate[i].tipoFermata == 'P'? "--:--" :getTime(info.fermate[i].arrivoReale), //Arrivo
      departure: info.fermate[i].tipoFermata == 'A'? "--:--" :getTime(info.fermate[i].partenzaReale), //Partenza
      track: info.fermate[i].tipoFermata == 'P'? info.fermate[i].binarioEffettivoPartenzaDescrizione :info.fermate[i].binarioEffettivoArrivoDescrizione, //Binario
    }
    ret.status.push(fermata);
  }
  return ret;
}

function getTime(millis) {
  var d = new Date(millis);
  var minutes = (d.getMinutes()<10?'0':'') + d.getMinutes();
  return millis == null? "--:--" : d.getHours() + ":" + minutes;
}

exports.getNews = function() {
  var endpoint = "/news/0/it";
  return JSON.parse(get(url_train_info, endpoint, ""));
}

exports.getStationDepartures = function(req) {
  var endpoint = "/partenze/"+req.cod_stazione+"/";
  return JSON.parse(get(url_train_info, endpoint, new Date()));
}

exports.getStationArrivals = function(req) {
  var endpoint = "/arrivi/"+req.cod_stazione+"/";
  return JSON.parse(get(url_train_info, endpoint, new Date()));
}
