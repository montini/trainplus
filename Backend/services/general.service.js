var User = require('../models/user');
var bcrypt = require('bcrypt');
_this = this

exports.createUser = async function(user){
    var newUser = new User({
      email: user.email,
      username: user.username,
      password: user.password,
      passwordConf: user.passwordConf,
    });
    try{
      var savedUser = await User.create(newUser);
      return savedUser;
    }catch(e){
        throw Error("Error while Creating User")
    }
}

exports.logout = async function(req) {
  if (req.session) {
    try {
      var ret = await req.session.destroy();
      if(ret) return true;
    }catch(e){
      console.log(e);
        throw Error("Error while logging out User");
    }
  } else {
    var err = new Error('Session not present');
    err.status = 401;
    return err;
  }
}

exports.getLoggedUser = async function(userId) {
  try {
    var authUser = await User.findOne({ _id: userId});
    return authUser;
  } catch(e){
      throw Error("Error while getting User")
  }
}

exports.authenticateUser = async function(user, req){
  var userData = {
    logemail: user.logemail,
    logpassword: user.logpassword,
  }
  try {
    var authUser = await User.findOne({ email: userData.logemail});
    var psw = await passwordHashUser (user, authUser);
    if(psw) {
      return authUser;
    } else {
      throw Error("User don't match");
    }
  } catch(e){
      throw Error("Error while authenticate User")
  }
}

async function passwordHashUser (user, authUser) {
  try {
    var id = await bcrypt.compare(user.logpassword, authUser.password);
    return id;
  }catch(e){
      throw Error("Error while authenticate User");
  }
}

exports.deleteUser = async function(id){

    try{
        var deleted = await User.remove({_id: id})
        if(deleted.n === 0){
            throw Error("Todo Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting user")
    }
}
